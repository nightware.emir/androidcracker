package com.example.tools;

import android.util.Base64;
import tgio.rncryptor.RNCryptorNative;

public class Cipher {

    public static final String KEY = "MY:STRONG:SECRETS:KEY";
    private static RNCryptorNative rncryptor = new RNCryptorNative();
    public static final Cipher INSTANCE = new Cipher();

    public final RNCryptorNative getRncryptor() {
        return rncryptor;
    }

    public final void setRncryptor(RNCryptorNative rNCryptorNative) {
        rncryptor = rNCryptorNative;
    }

    public final String encryptNat(String src) {
        byte[] encrypt = rncryptor.encrypt(src, decryptBase64(KEY));
        return new String(encrypt);
    }

    public final String decryptNat(String src) {
        return rncryptor.decrypt(src, decryptBase64(KEY));
    }

    public final String encryptBase64(String string) {
        byte[] encodeValue = Base64.encode(string.getBytes(), 0);
        return new String(encodeValue);
    }

    public final String decryptBase64(String string) {
        byte[] decodeValue = Base64.decode(string, 0);
        return new String(decodeValue);
    }
}
