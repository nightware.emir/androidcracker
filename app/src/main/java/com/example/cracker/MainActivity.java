package com.example.cracker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tools.Cipher;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    TextView textResult;
    Button btnCrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textResult = (TextView) findViewById(R.id.txtView);
        editText = (EditText) findViewById(R.id.editTextoCifrado);
        btnCrack = (Button) findViewById(R.id.btnCrack);

        btnCrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textoACifrar = editText.getText().toString();
                String textoCifrado = Cipher.INSTANCE.decryptNat(textoACifrar);

                textResult.setText(textoCifrado);
            }
        });
    }
}